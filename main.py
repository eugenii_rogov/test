import uvicorn
from fastapi import FastAPI, status
from fastapi.responses import JSONResponse
from dotenv import load_dotenv
from pydantic import BaseModel

from src.tools.config import Config
from src.tools.data_holder import DataHolder


class GetReq(BaseModel):
    build: str


app = FastAPI()


@app.post("/build/")
async def get_build_tasks(get_req: GetReq):
    """
    Получить задачи для сборки билда в порядке их установки
    :param get_req: Модель запроса
    :return: Задачи для сборки
    """
    try:
        build = data_holder.builds.get(get_req.build)
        if build:
            res = build.execute()
            for task in res:
                data_holder.tasks[task].set_unready()
            return JSONResponse(status_code=status.HTTP_200_OK, content=res)
        else:
            return JSONResponse(status_code=status.HTTP_404_NOT_FOUND,
                                content={"message": f"Build {get_req.build} not found"})
    except:
        return JSONResponse(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            content={"message": "Internal Server Error"})

if __name__ == "__main__":
    load_dotenv()
    config = Config()
    data_holder = DataHolder(builds_path=config.builds_path,
                             tasks_path=config.tasks_path)
    uvicorn.run(app)
