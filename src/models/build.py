from typing import List

from src.tools.logger import get_logger
from src.models.tasks_queue import TasksQueue


class Build:
    """
    Класс билда
    """
    def __init__(self, 
                 name: str,
                 tasks_queue: TasksQueue) -> None:
        """
        Инициализация объекта
        :param name: Наименование
        :param tasks_queue: Задачи для выполнения
        """
        self._name = name
        self._tasks_queue = tasks_queue
        self._logger = get_logger(f"Билд {self._name}")

    def execute(self) -> List[str]:
        """
        Собрать билд
        :return: Задачи в порядке их выполнения
        """
        res = []
        for task in self._tasks_queue:
            res.extend(task.execute())
        self._logger.info("Билд собран.")
        return res
