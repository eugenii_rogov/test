from typing import List

from src.tools.logger import get_logger
from src.models.tasks_queue import TasksQueue


class Task:
    """
    Класс задачи
    """
    def __init__(self,
                 name: str,
                 dependencies: TasksQueue | None = None) -> None:
        """
        Инициализация объекта
        :param name: Наименование
        :param dependencies: Зависимости
        """
        self._name = name
        self._dependencies = dependencies if not None else TasksQueue([])

        self._is_ready = False
        self._logger = get_logger(f"Задача {self._name}")

    def execute(self) -> List[str]:
        """
        Выполнить задачу
        :return: Задачи в порядке их выполнения
        """
        res = []
        if not self.is_ready:
            if self.dependencies:
                for task in self.dependencies:
                    if task.is_ready:
                        continue
                    res.extend(task.execute())
            self.set_ready()
            res.append(self._name)
            self._logger.info("Задача выполнилась.")
        return res

    def set_ready(self) -> None:
        """
        Установка флаг готовности
        :return: None
        """
        self._is_ready = True

    def set_unready(self) -> None:
        """
        Установка неготовности
        :return: None
        """
        self._is_ready = False

    def set_dependencies(self, dependencies: List["Task"]) -> None:
        """
        Усиановить зависимости
        :param dependencies: Зависимости
        :return: None
        """
        if self._dependencies:
            self._logger.warning("Произошла перезапись зависимостей")
        self._dependencies = TasksQueue(dependencies)

    @property
    def name(self) -> str:
        """
        Получить наименование задачи
        :return: Наименовение задачи
        """
        return self._name

    @property
    def dependencies(self) -> TasksQueue:
        """
        Получить зависимости
        :return: Зависимости
        """
        return self._dependencies

    @property
    def is_ready(self) -> bool:
        """
        Получить статус готовности
        :return: Статус
        """
        return self._is_ready

    def __repr__(self):
        return f"Задача {self._name}"
