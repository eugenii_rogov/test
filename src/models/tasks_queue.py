from __future__ import annotations
from typing import List


class TasksQueue:
    """
    Очередь зависимостей для установки
    """
    def __init__(self, 
                 data: List["Task"] | None = None) -> None:
        """
        Очередь установки зависимостей
        :param data: Набор заданий
        """
        self._data = data if data else []

    def __next__(self) -> "Task":
        if self._n < len(self._data):
            v = self._data[self._n]
            self._n += 1
            return v
        else:
            raise StopIteration()

    def __iter__(self):
        self._n = 0
        return self
    
    def __len__(self) -> int:
        return len(self._data)
