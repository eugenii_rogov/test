import os


class Config:
    """
    Класс конфига приложения
    """
    def __init__(self):
        """
        Инициализация объекта
        """
        self._tasks_path = os.getenv("TASKS_PATH", "")
        self._builds_path = os.getenv("BUILDS_PATH", "")

    @property
    def tasks_path(self) -> str:
        """
        :return: Путь задач
        """
        return self._tasks_path

    @property
    def builds_path(self) -> str:
        """
        :return: Путь билдов
        """
        return self._builds_path
