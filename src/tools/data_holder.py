from typing import Dict, List, Literal

from src.models.tasks_queue import TasksQueue
from src.models.task import Task
from src.models.build import Build

from src.tools.parser import Parser
from src.tools.logger import get_logger


class DataHolder:
    """
    Класс Датахолдера
    """
    def __init__(self,
                 builds_path: str,
                 tasks_path: str):
        """
        Инициализация объекта
        :param builds_path: Путь билдов
        :param tasks_path: Путь заданий
        """
        self._logger = get_logger("DataHolder")
        parser = Parser(builds_path=builds_path,
                        tasks_path=tasks_path)
        self._builds = {}
        self._tasks = {}

        self._initialize_tasks(parser.tasks)
        self._initialize_builds(parser.builds)

    def _initialize_tasks(self,
                          tasks: List[Dict[Literal["name"] | Literal["dependencies"], str | List[str]]]) -> None:
        """
        Инициализация заданий
        :param tasks: Задания
        :return: None
        """
        self._tasks: Dict[str, Task] = {task["name"]: Task(task["name"]) for task in tasks}
        for task in tasks:
            t_name, t_depends = task["name"], task["dependencies"]
            if t_depends:
                deps = self._task_objects(t_depends)
                self._tasks[t_name].set_dependencies(deps)

    def _initialize_builds(self,
                           builds: List[Dict[Literal["name"] | Literal["tasks"], str | List[str]]]) -> None:
        """
        Инициализация билдов
        :param builds: Билды
        :return: None
        """
        for build in builds:
            b_name, b_tasks = build["name"], build["tasks"]
            deps = self._task_objects(b_tasks)
            self._builds[b_name] = Build(b_name, TasksQueue(deps))

    def _task_objects(self,
                      tasks: List[str]) -> List[Task]:
        """
        Получить объекты заданий
        :param tasks: Наименования заданий
        :return: Объекты заданий
        """
        deps = []
        for dep_t in tasks:
            if dep_t not in self._tasks:
                self._logger.warning(f"Задание {dep_t} не найдено")
                continue
            deps.append(self._tasks[dep_t])
        return deps

    @property
    def builds(self) -> Dict[str, Build]:
        return self._builds

    @property
    def tasks(self) -> Dict[str, Task]:
        return self._tasks
