import logging
import sys

_log_format = "%(asctime)s - [%(levelname)s] |%(name)s| %(filename)s:%(lineno)d (%(funcName)s): %(message)s"


def get_logger(name: str) -> logging.Logger:
    handlers = [logging.StreamHandler(sys.stdout)]
    level = logging.INFO

    logging.basicConfig(
        format=_log_format,
        level=level,
        handlers=handlers
    )
    logger = logging.getLogger(name)
    return logger
