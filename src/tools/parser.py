import yaml
import os.path
from typing import Dict, List, Literal
from src.tools.logger import get_logger


class Parser:
    """
    Класс парсера
    """
    def __init__(self,
                 builds_path: str,
                 tasks_path: str):
        """
        Инициализация объекта
        :param builds_path: Путь билдов
        :param tasks_path: Путь заданий
        """
        self._logger = get_logger("Parser")
        if os.path.exists(builds_path):
            with open(builds_path, "r") as stream:
                try:
                    self._builds = yaml.safe_load(stream)
                    self._logger.info("Получены билды")
                except yaml.YAMLError as e:
                    raise e
        else:
            raise FileNotFoundError("Файл билдов не найден")

        if os.path.exists(builds_path):
            with open(tasks_path, "r") as stream:
                try:
                    self._tasks = yaml.safe_load(stream)
                    self._logger.info("Получены задания")
                except yaml.YAMLError as e:
                    raise e
        else:
            raise FileNotFoundError("Файл заданий не найден")

    @property
    def tasks(self) -> List[Dict[Literal["name"] | Literal["dependencies"], str]]:
        """
        :return: Задания
        """
        return self._tasks["tasks"]

    @property
    def builds(self) -> List[Dict[Literal["name"] | Literal["tasks"], str]]:
        """
        :return: Билды
        """
        return self._builds["builds"]
