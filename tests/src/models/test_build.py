from src.models.build import Build
from src.models.task import Task
from src.models.tasks_queue import TasksQueue as tq


def test_execute():
    t11 = Task("t11", tq([]))
    t10 = Task("t10", tq([t11]))
    t5 = Task("t5", tq([]))
    t4 = Task("t4", tq([t5]))
    t3 = Task("t3", tq([t4, t10]))
    t2 = Task("t2", tq([t3]))
    t8 = Task("t8", tq([]))
    t7 = Task("t7", tq([t8]))
    t6 = Task("t6", tq([t7]))
    b = Build("b", tq([t2, t3, t4, t6]))
    res = b.execute()
    assert res == ['t5', 't4', 't11', 't10', 't3', 't2', 't8', 't7', 't6']
