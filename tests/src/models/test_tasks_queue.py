from src.models.tasks_queue import TasksQueue
from src.models.task import Task


def test_creation():
    tq1 = TasksQueue()
    tq2 = TasksQueue([Task("T1", TasksQueue()), Task("T2", TasksQueue())])
    assert len(tq1) == 0
    assert len(tq2) == 2
    assert bool(tq1) is False
    assert bool(tq2) is True

